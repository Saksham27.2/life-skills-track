#### In this video, what was the most interesting story or idea for you?
 The best idea here is to start with small habits instead of trying big changes all at once. There are three parts to habits: the cue (what starts it), the routine (what you do), and the reward (what you get). By just slightly changing one of these, you can form good habits more easily. It's a great way to make habits.

 #### How can you use B = MAP to make making new habits easier? What are M, A and P. 
 B' is my behavior, like working out. 'M' is motivation – why do I want to exercise? The clearer my reason, the better. 'A' is ability – make it super easy, like a short workout. Finally, 'P' is prompt – set a reminder or link it to something I already do, like exercising after brushing my teeth. Following B = MAP makes forming new habits way easier!"

 #### Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

  When you feel good about doing something, you're more likely to keep doing it. BJ Fogg says if you celebrate after each small win, it becomes a habit. So, by adding a happy moment after doing a habit, you're telling your brain, "Hey, this is good!" and it helps the habit stick.

#### In this video, what was the most interesting story or idea for you?

I feel most interesting part in this when you are making tiny betterments in your life,
like 1% better, in everything you do. It's like how a cycling team got way better by making small changes in lots of areas. This shows that even small habits and improvements can add up to something really big over time. So, the message is, don't underestimate the power of small, consistent steps—they can lead to major success!

#### What is the book's perspective about Identity?

The book "Atomic Habits" says tto make yourself stick to the good habits, think about becoming the kind of person who does those habits. It's not just about reaching goals; it's about making habits a part of who you are. Small changes add up over time, so focus on being consistent and creating a positive identity around your habits.

#### Write about the book's perspective on how to make a habit harder to do?

This book doesnot refers about harder things but instead you can focusing on creating an environment that makes good habits easier and bad habits more difficult. For instance, if you want to stop snacking, you might keep unhealthy snacks out of reach. The idea is to set up your surroundings to help with good habits and make it a bit tough for bad ones.

#### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I would like to take care of my health more often in terms of eating,sleeping ,lifting weights and to more focus on myself .
 ###### To make it attractive
 - I am pushing myself to the gym for lifting weights and taking care of what i am eating is healthy and seeing myself after a week ,I am lifting more weight than last week which helps me to go to gym everyday.


 #### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

 I would like to eliminate my habit of procrastinating things and overthinking .

 ###### Step ->
 - I am engaging myself into small work so that i wont be idle for longer time and in this way ,I am able to reduce my procrastinating and ovethinking. 