#### What are the activities you do that make you relax - Calm quadrant?
- Taking breaks from work whenever needed
- Walking in a park or nature for relaxation of mind 
- Listening to music for calming mind thoughts 
- Writing down the thoughts for clarity of mind

#### When do you find getting into the Stress quadrant?
- Dealing with tight deadlines or unexpected changes.
- Facing tough tasks or high expectations.
- Doing intense workouts.
- Learning new things in a short period of time.

#### How do you understand if you are in the Excitement quadrant?
- When I am full of energy before starting the day.
- I smile and laugh more.
- I'm open to new ideas and opportunities.

#### Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.
- Sleep is crucial for body and brain health.
- Lack of sleep can harm health, increase heart rates, and cause accidents.
- Poor sleep weakens the immune system, leading to more infections.
- Sleep is vital for memory, learning, and overall well-being.
- A study found disrupted gene activity with only four hours of sleep.


#### What are some ideas that you can implement to sleep better?
- Going to bed and wake up at the same time every day.
- To Make Bedroom cool, dark, and quiet for better sleep.
- By Avoid Screens Before Bed.
- By Avoid heavy meals and too many drinks before bedtime.

#### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

- One-minute exercise can contribute to brain health.
- Exercise brings long-lasting benefits.
- Guards against neurodegenerative diseases.
- Minimal exercise boosts mood, attention, and memory.
- Aim for 30 minutes of daily exercise, including some aerobic activity.


#### What are some steps you can take to exercise more?

- Include some aerobic activities in my routine.
- Find activities that I enjoy to make exercise more enjoyable.
- Aim for at least 30 minutes of daily exercise.
Set realistic goals to stay motivated.
- Make it a social activity by exercising with friends or family.