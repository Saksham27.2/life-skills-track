#### What are the steps/strategies to do Active Listening?

-Avoid Getting Distracted by your own thoughts
-Focus on the speaker and topic
- Try not to interrupt the other person while speaking
- Use Door Openers ,These are the phrases which show you are interested and keep the other person talking.
- Show that you are listening by body language .
- Take notes during important conversations

#### According to Fisher's model, what are the key points of Reflective Listening? 

- Useful in personal relationships, particularly in helping others open up about their feelings.
-  Emerged from Carl Rogers' therapy, emphasizing genuine understanding and empathy.
-  Reflective listening involves confirming the speaker's idea to ensure correct understanding.

#### What are the obstacles in your listening process?
- Distractions
- Prejudgment
- Selective Listening

#### What can you do to improve your listening?
- Showing that I am Interested in their talk
- Avoid Making Quick Judgments about the other person
- Will try to Being Kind and Understand to Others.


#### When do you switch to Passive communication style in your day to day life?
I usually use a passive communication style when I want to keep things friendly and avoid conflicts. While I believe in open communication, there are times when I prioritize maintaining positive relationships over expressing my thoughts assertively.

#### When do you switch into Aggressive communication styles in your day to day life?

 I might switch to an aggressive communication style when I'm feeling overwhelmed with work or not in a good mood. During these times, I might express my thoughts forcefully or react more strongly than usual.

 #### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

  I find myself adopting passive-aggressive communication styles, like sarcasm, gossiping, taunts, or the silent treatment, when I'm feeling frustrated or resentful. This tends to happen when I'm unwilling or unable to express my feelings directly.

  #### How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

  
- I'll tell how I feel, not who's to blame. I'll be honest about my emotions.

- I'll practice speaking up in simple situations to gain confidence.

- I'll pay attention to my body language and tone. I'll make sure they match what I'm saying.

- I won't wait to talk about problems. I'll speak up early to fix things faster thereby preserving relationships




