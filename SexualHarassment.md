### Sexual Harassment ?

-Any unwelcome verbal,visual or sexual conduct of sexual nature that is severe and affects working condition 

#### Q1 What kinds of behaviour cause sexual harassment ?

There are 3 forms of sexual harassment which includes 


1.Verbal 

It includes commenting on clothing or body of an individual,Repeatedly asking a person out  ,Sexual or gender based jokes,spreading rumors about a person's personal or professional life.

2.Visual

It includes  obscene wallpapers/Screensavers on personal device,Email/Texts of sexual nature.

3.Physical Harassment

It includes Sexual Assault,inappropriate touching such as rubbing,kissing,hugging without consent also sexual gesturing ,staring.


These behaviors are the cause of sexual harassment.


#### Q2 What would you do in case you face or witness any incident or repeated incidents of such behaviour? 

If i were to face or witness any repeated incidents of sexual harassment  

1.Take note of the date,time,location and details of the incident and will document the incident

2.If i am comfortable to do the reprting the incident,I would report to the appropriate authorities within the organization.

3.Reaching out to trusted friends,family members or colleagues for emotional support.

4.If the harassment persists or if the organisation fails to take appropriate action,consider seeking legal advice from an attorney specialising harassment cases.


