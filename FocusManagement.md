#### What is Deep Work?
Deep work means intensely focusing on challenging tasks without distractions. It's like putting your brain in hard focus mode, such as programming, where you're fully immersed. This undistracted concentration is vital for success in jobs that require a lot of thinking, like theoretical work, according to Cal Newport and Lex Fridman.

#### According to author how to do deep work properly, in a few points?
-  Schedule specific times daily or weekly for focused work.


- Begin with short sessions; aim for up to four hours daily over time.


- Plan tasks for the next day in the evening to ensure good sleep and focused work.

#### How can you implement the principles in your day to day life?
-  I block off specific hours in my daily or weekly schedule for concentrated work.


-  I begin with shorter focused sessions and gradually increase the duration over time.


-  I establish an evening routine to plan and organize tasks for the next day, promoting better sleep and preparedness for meaningful work.

#### What are the dangers of social media, in brief?

- Social media is designed to grab and keep our attention, often making it hard to focus on other things.

-  Using social media too much can make you feel bad and even cause anxiety.

-  Social media is made to be addictive, which can be a problem for our brains.

-  Stopping social media can be tough initially, but after two weeks, it gets easier, and life without it can be better.