#### Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

-  In this video, Angela Duckworth says that being passionate and not giving up is super important for doing well in school and life. She found that it's not about how smart you are or how you look, but about sticking with things and thinking of life like a marathon, not a sprint. Grittier students, who don't give up easily, are more likely to finish school.


#### Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.
- Growth mindset is thinking you can become smarter by trying hard. It helps you learn and get better. You might sometimes think you can't do something, but understanding this lets you change and improve. It's different from a fixed mindset, where you believe your abilities are fixed and can't change.

#### What is the Internal Locus of Control? What is the key point in the video?
- In the video, a study at Columbia University showed that fifth graders who attributed success to hard work focused on easier tasks and were less persistent on difficult ones. On the other hand, those who attributed success to intelligence were more motivated and persistent. The key point is the video provides advice on adopting an internal locus of control to stay motivated.

#### What are the key points mentioned by speaker to build growth mindset.?
- Learn from Failures
- Question Your Assumptions
- Develop a Curriculum for Dreams
- Honor the Struggle

#### What are your ideas to take action and build Growth Mindset?
-  I believe in myself that I can do and work on feedback received from others, learning from my failures as well./