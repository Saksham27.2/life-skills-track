##### What is the Feynman Technique? Explain in 1 line.

If you cant explain simpler,you didnot understand it. 

#### In this video, what was the most interesting story or idea for you?


In her TED talk, Barbara Oakley shares  her personal challenges with mathematics and science before discovering effective learning strategies. She introduces the concept of two distinct modes of thinking: "focused/active" and "diffuse," illustrating their significance in the learning process. Drawing upon examples from the realms of art and innovation, Oakley underscores the impact of these modes on cognitive processes. She also addresses the common obstacle of procrastination and proposes methods for overcoming it. Oakley introduces the Pomodoro Technique as a tool for enhancing focused work and stresses the critical role of testing, practice, and recall in the learning journey.

#### What are active and diffused modes of thinking?

- Focused/Active mode: This is when you're actively concentrating on a task, like solving a problem or learning something specific.
Example: Analyzing a piece of literature with intense focus.

- Diffuse Mode of Thinking: This is a more relaxed state where your mind is free to wander. It's helpful for making creative connections and seeing the bigger picture.
Example:  allowing your thoughts to wander during a  bike ride.

#### According to the video, what are the steps to take when approaching a new topic? Only mention the points.

- Practice for at least 20 hours
- Learn enough to practice 
- Break down the skill into Small and understandable pieces.
- Understanding and remove barriers to practice.

#### What are some of the actions you can take going forward to improve your learning process?

- Understand Focus/active and diffuse Modes
- Strategic Mode Switching
- Break Down Learning Tasks
- Learn from Examples
- Recall Information 