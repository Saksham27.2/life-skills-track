#### Which point(s) were new to you?
- Getting frequent feedback to ensure I am on track and everyone is on the same page.


- Using tools like TimeLimit and Freedom to remove notifications during work hours.


- Being available when someone replies to your message.

#### Which area do you think you need to improve on? What are your ideas to make progress in that area?
- Area for Improvement:-

Better Team Communication in Meetings
Taking notes while discussing requirements with your team.

- Idea to make Progress :-

Ask Questions in Meetings
Use Group Chat More
Explain Problems Clearly
Use Pictures and Videos to make explanations clearer.
Share code using Github gists and use sandbox tools.